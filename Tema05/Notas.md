# Notas del Tema 05 Parte 1 de Arquitectura en Computadoras

# Introducción
El conjunto de instrucciones es la porción visible de una computadora para el 
programador o compilador. Las notas nos presenta una amplia variedad de diseños
y alternativas posibles para la arquitectura del conjunto de instrucciones. En
particular nos enfocaremos en 4. 
Primero, presentaremos una taxonomía para las alternativas en el conjunto de 
instrucciones y daremos algunas evaluaciones cualitativas de las ventajas y
las desventajas de varios enfoques. 
Segundo, presentaremos y analizaremos algunos conjuntos de instrucciones que son
en gran medida independientes de un conjunto de instrucciones especifico.
Tercero, abordaremos el problema de los lenguajes, compiladores y su soporte
en la arquitectura del conjunto de instrucciones.
Finalmente, mostraremos como estas ideas reflejan en el conjunto de 
instrucciones MIPS, el cual es común de las arquitecturas RISC. 
Concluiremos con las falacias y fallas del diseño de conjunto de instrucciones.

El Apendice K da 4 ejemplos de un propósito general de las arquitecturas RISC (
MIPS, PowerPC, Precision Architecture, SPARC), 4 mejoras agregadas por los 
procesadores RISC (ARM, Hitachi SH, MIPS 16, Thumb) y 3 arquitecturas viejas (
80x86, IBM 360/370 y VAX). Antes de discutir sobre cómo se clasifican las 
arquitecturas, necesitamos decir algo acerca de las medidas en el conjunto de 
instrucciones. 

A través de este apendice, examinaremos una amplia variedad de arquitecturas de
medidas. Claramente estás medidas depende de los programas en los que son 
medidos y en los compiladores usados para estás mediciones. El resultado no 
debería de interpretarse como absoluto y deberíamos ver la diferencia en 
información si hicimos la medida con un compilador o programa diferente.
Creemos que las medidas en este apendice, son razonablemente indicativas para
una clase tipica de aplicación. Varias medidas son presentadas usando pequeños
sets de benchmarks, de esta forma la información desplegada y su diferencia con
los programados puede ser vista. Una arquitectura para una nueva computadora 
debe analizar una colección extensa de programas antes de realizar decisiones
arquitectonicas.
Las medidas mostradas suelen ser dinámicas, esto es, la frecuencia medida se 
pesa por el número de veces que un evento ocurre durante la ejecución del 
programa.

Antes de empezar con los principios generales, debemos dar una revisión a las 
tres ares de aplicación que existen (vistas en el capítulo 1): 
- Desktop Computing: Enfatiza el rendimiento de los programas con información de
enteros y puntos flotantes y con una poca consideración en el tamaño del 
programa.
- Servers: Usados principalmente para las bases de datos y aplicaciones web, 
además de algunas aplicaciones de tiempo compartido. Por esto, el rendimiento
del punto flotante es menos importante que el rendimiento de enteros y cadenas,
a pesar de esto, virtualmente cada proceso del servidor sigue incluyendo 
instrucciones de punto flotante.
- Personal mobile devices and embedded applications: Ya que el valor se 
encuentra en el costo y en la energía, el tamaño del código es importante, ya
que menos memoria es más barato y consume menos energía y algunas clases de 
instrucciones (como el punto flotante) podría ser opcional para reducir el 
costo de producción de los chips.

Así, el conjunto de instrucciones para estas 3 aplicaciones es muy similar. De 
hecho la arquitectura MIPS ha sido satisfactoria para computadoras, servidores
y para aplicaciones integradas.

Una arquitectura satisfactoria muy diferente a RISC es la 80x86 (ver apendice 
K). Sorpresivamente, su éxito no necesariamente contradice las ventajas del
conjunto de instrucciones RISC. La importancia comercial de la compatibilidad
binaria con el software en PC combinada con la abundancia de los transistores le
permitió a INTEL usar internamente las instrucciones RISC mientras soportaban 
externamente las instrucciones 80x86. 
Pentium 4 usa hardware para traducir instrucciones de 80x86 a RISC y ejecuta
las instrucciones traducidas dentro del chip. Mantiene la ilusión de una 
arquitectura 80x86 para el programador mientras permite al diseñador 
computacional el implementar mejoras de un procesador RISC.

# Clasificando arquitecturas de conjuntos de instrucciones
El tipo de almacenamiento interno de un procesador es la más básica diferencia,
en este sección nos enfocaremos en alternativas para esta porción de la
arquitectura.
La mayor decisión son en un stack, un acumulador o un conjunto de registros.
Los operandos deberan ser llamados explicitamente o implicitamente: El 
operando en una arquitectura stack son implicitos en la cima del stack y en una
arquitectura de acumulador un operando es implicito en el acumulador. En las
arquitecturas de registro de proposito general tienen sólo operandos explicitos,
ya sea en los registros o en las locaciones de memoria.

Figura A.1 muestra un bloque del diagrama de estas arquitecturas. (Agregar foto 
a la presentación)
Figura A.2 muestra como la secuencia en código ```C = A + B``` aparecería en 
estas tres clases de conjunto de instrucciones. (Agregar foto a la presentación)

Los operadores explicitos pueden acceder directamente desde memoria o necesitan
ser cargados en un almacenamiento temporal, dependiendo de la clase de 
arquitectura y la elección especifica de la instrucción.

Como podemos ver en las imagenes, son realmente dos clases de registrar. Una 
clase puede acceder a memoria como parte de cualquier instrucción, llamada:
arquitectura de registro de memoria. El otro puede acceder a memoria sólo
con cargar y almacenar instrucciones llamada: arquitectura de carga y 
almacenamiento. Existe una tercer clase que no se encuentra en las computadoras
que se venden la cuál mantiene todos los operandos en memoria y es llamada:
arquitectura de memoria en memoria. Algunas conjuntos de arquitecturas de 
instrucciones tiene más registros que un sólo acumulador, algunas veces son 
llamadas: Acumuladores extendidos o registros de proposito especial.

Aunque la mayoria de las primeras computadoras usaban arquitecturas stack o
arquitecturas de acumulador, virtualmente cada nueva arquitectura diseñada
después de 1980 usa una arquitectura de carga y almacenamiento. La razón por la
que la arquitectura de registro de proposito general emergió es por dos razones.

Primero, los registros al igual que otras formas de almacenamiento interno del
procesador son más rápidas que la memoria. Segunda, los registros son más
eficientes para un compilador que usar otras formas de almacenamiento interno.

Por ejemplo en una computadora con registros, la expresión `(A*B)-(B*C)-(A*D)` 
deberá ser evaluada haciendo las multiplicaciones en cualquier orden, lo cual 
sería más eficiente debido a la locación de los operados o por las pipelining
concernes.
A pesar de eso, en una computadora con stack el hardware deberá evaluar la 
expresión sólo en un orden, ya que los operados están escondidos en el stack, 
eso cargaría los operandos multiples veces.

Más importante, los registros pueden ser usados para mantener variables. Cuando
las variables están alojadas en los registros, el trafico de memoria se reduce,
el programa aumenta su velocidad y la cantidad de código también mejora.
(El programa aumenta su velocidad debido a que los registros son más rápidos que
la memoria y el código disminuye porque cada registro puede ser nombrado con 
pocos bits en vez de una localización de memoria).

Los escritores de compiladores prefieren que todos los registros sean 
equivlente y no-reservados. Computadoras antiguas se comprometen con este deseo
dedicando registros para usos especiales, decrementando efectivamente el número
de registros de proposito general. Si el número de registros de proposito 
general es muy pequeño, trata de alojar variables en registros que no serán
utiles. En lugar de que el compilador reserve todo los registros no 
comprometidos para usarlos en la evaluación de la expresión.

* ¿Cuántos registros son suficientes? 
La respuesta es que depende de la efectividad del compilador. Varios 
compiladores reservan algunos registros para evaluar expresiones, usan algunos
para pasar los parametro y permiten el resto para alojar variables que se
matendrán. Compiladores modernos tienen la habilidad de usar largos números de
registros de manera efectiva.

Dos grandes conjuntos de instrucciones dividen las arquitecturas GPR (registros
de proposito general). Ambas caractericasticas tienen que ver con la naturaleza
de los operandos para una aritmetica tipica o instrucciones lógicas 
(instrucciones ALU). El primero tiene que ver con que las instrucciones ALU 
tienen dos o tres operandos. En el formato de tres operandos, la instrucción
contiene un operando resultado y dos operandos fuente. En el formato de dos
operadons, uno de los operandos es ambos tanto fuente como resultado de la
operación. La segunda distinción de la arquitectura GPR tiene que ver con 
cuantos de los operandos deben estar en la dirección de memoria de las 
instrucciones ALU.

El número de operandos de memoria admitidos por una instrucción ALU típica puede
variar desde ninguno hasta tres. La figura A.3 muestra combinaciones de estos
dos atributos con ejemplos de computadoras. Aunque hay siete combinaciones
posibles, tres sirven para clasificar casi todas las computadoras existentes.
Como mencionamos anteriormente, estos tres son de arquitectura 
almacenamiento-memoria (también llamado registro-registro), registro-memoria y
memoria-memoria. (Agregar figura A.3 en la presentación).

La figura A.4 muestra las ventajas y las desventajas de cada una de estas 
alternativas. Por supuesto, las ventajas y desventajas no son absolutas: Son
cualitativas y su impacto actual depende del compilador y de la estrategia de
implementación. Una computadora con arquitectura GPR con operaciones de 
memoria-memoria podría ser facilmente ignorada por el compilador y usada como
una computadora con arquitectura almacenamiento-memoria. Una de las 
arquitecturas que más ha permeado es en instrucciones de codigicación y el 
número de intreucciones necesitadas para la realización de la tarea. Vemos el 
impacto de estas alternativas de implementaciones en el Apendice C y Capítulo 3.

# Acceso de memoria
Independientemente de que si la arquitectura es de almacenamiento-memoria o 
permite cualquier operando para ser una referencia en memeoria, debe ser 
definida en como el acceso de memoria es interpretado y como se especifica.
Las medidas presentadas aquí son amplias, pero no completas. En algunos casos
las medidas son afectadas significativamente por la tecnología del compilador.

Estas medidas han sido hecas usando un compilador optimizado, hay que tener en 
cuenta que la tecnología del compilador juega un rol crítico.

# Interpretando el acceso en memoria
* ¿Cómo se interpreta un acceso de memoria? ¿Qué objeto es accedido como una 
función de la dirección y su longitud? 
Todo el conjunto de instrucciones discutido en este libro son accesos de bytes y
proveen acceso para bytes (8 bits), palabras a medias (16 bita), y palabras 
(32 bits). La mayoría de las computadoras también proveen acceso para palabras
dobles (64 bits).

Hay dos convenciones diferentes para ordenar los bytes entre lo largo de su 
objeto. 
* Little Endian: El orden de los bytes pone al byte cuya dirección es "x...x000"
en la posición menos significante. Los bytes son numerados:
        
        7 6 5 4 3 2 1 0

* Big Endian: El orden de los bytes cuya dirección es "x...x000" está en la 
posición más significante. Los bytes son numerados:

        0 1 2 3 4 5 6 7

Cuando se opera con una computadora, el orden de bytes es imperceptible, sólo
programas que acceden a las mismas locaciones palabras y bytes, pueden observar 
la diferencia. El ordenamiento de bytes es un problema cuando intercambiamos 
información a través de computadoras con diferentes ordenamientos.
El ordenamiento Little Endian también falla al comparar el ordenamiento normal
de palabras cuando las cadenas son comparadas. Los strings aparecen como:
"SDRAWKCAB" (backwards) en los registros.
Un segundo problema de memoria es que en varia computadras, el acceso a objetos
más largos que un byte son desalineados. Un acceso a un objeto de tamaño s bytes
a la dirección de byte A es alineado si `A mod s = 0`. La figura A.5 muestra 
como las direcciones a las cuales un acceso es alineado o desalineado (mostrar 
en la presentación la imagen).

* ¿Por qué alguien debería diseñar una computadora con restricciones de 
alineación?

El mal alineamiento causa complicaciones de hardware ya que la memoria está
tipicamente alineada en un limite de múltiples palabras o palabras doble. Un
mal alineamiento del acceso de memoria podría tomar multiples referencias a 
memorias alineadas. Así, incluso una computadora que permite acceso a mal
alineamientos, los programas con accesos alineados se ejecutan más rápido.
Incluso si la información esta alineada, soportar el acceso a byte, 
media-palabra y palabra requiere un alineamiento en la conexión para 
alinear byte, media-palabras y palabras en registros de 64bit.
Por ejemplo en la figura A.5, supongamos que necesitamso leer un byte desde una
dirección con 3 bits de bajo orden, obteniendo el valor 4. Nosotros necesitamos
desplazarnos 3 bytes para alinear el byte a su correspondiente lugar en un 
registro de 64-bit. Dependiendo de la instrucción, la computadora podría 
necesitar extender la cantidad.
El almacenamiento es sencillo: Sólo el acceso en la memoría de los bytes puede 
ser alterado. En algunas computadoras la operación no afecta la parte superior
de un registro.
Aunque todas las computadoras que discutimos permiten el acceso de memoria al 
byte, media-palabra y palabra, sólo la IBM 360/370, Intel 80x86 y VAX soportan
operaciones ALU en los operandos de los registros más angosto que el ancho 
completo.
Áhora que hemos discutivo las distintas interpretaciones del acceso de memoria,
podemos discutir las maneras en las que se dirigin por instrucciones, llamadas:
modos de direccionamiento/acceso.

# Modos de direccionamiento/acceso
Dada una dirección, nosotros ahora sabemos qué bytes acceden a la memoria. En 
está subsección, veremos los modos de acceso, cómo las  arquitecturas 
especifican la dirección de un objeto al que accederan. Los modos de acceso 
especifican constantes y registros en adición para las locaciones en memoria.
Cuando una locación de memoria es usada, la dirección actual de memoria es 
especificada por el modo de direccionamiento o acceso llamado effective address.
La figura A.6 muestra todos los modos de direccionamiento de la información que 
ha sido usada en recientes computadoras. Inmediatas o literales son usualmente
consideradas como modos de direccionamiento (aunque al valor que acceden está en
el flujo de instrucciones) y aunque los registros esten separados ellos 
normalmente no tienen direccionamiento de memoria.
Necesitamos mantener los modos de direccionamiento que depende del contador del
programa, llamado PC-relative addressing separado. PC-relative addressing es 
usado principalmente para especificar código de acceso en instrucciones de 
transferencia de constrol, discutidas en la Sección A.6.
La figura A.6 muestra los nombres más comunes para el modo de direccionamiento,
aunque los nombres difieren entre arquitecturas. 

-Esto agregarlo a una ficha para explicarlo en la presentación-
In this figure, only one non-C feature is used: The left arrow (←) is used
for assignment. We also use the array Mem as the name for main memory and the
array Regs for registers. Thus, Mem[Regs[R1]] refers to the contents of the 
memory location whose address is given by the contents of register 1 (R1). 
Later, we will introduce extensions for accessing and transferring data smaller
than a word.

---------------------------------------------------------------------------------

Los modos de direccionamiento tienen la habilidad de reducir el conteo de 
instrucciones significativamente; también añaden complejidad en la construcción
de una computadora e incrementan el promedio de ciclos de reloj por instrucción
(CPI) de las compuitadoras en las que son implementadas. Así, el uso de varios
modos de redireccionamiento es importante para elegir la arquitectura que 
queremos incluir.

La figura A.7 muestra el resultado de medir los modos de direccionamiento usando
patrones en tres programas en la arquitectura VAX. Usamos la vieja arquitectura
VAX para pocas medidas, porque teiene el conjunto de direcciones más completo y
las menores restricciones de direccionamiento de memoria.

-Explicar esto en una hojita personal y poner sólo las imágenes-
Figure A.7 shows the results of measuring addressing mode usage patterns in
three programs on the VAX architecture. We use the old VAX architecture for a
few measurements in this appendix because it has the richest set of addressing
modes and the fewest restrictions on memory addressing. For example, Figure A.6
on page A-9 shows all the modes the VAX supports. Most measurements in this
appendix, however, will use the more recent register-register architectures to 
show how programs use instruction sets of current computers.
As Figure A.7 shows, displacement and immediate addressing dominate
addressing mode usage. Let’s look at some properties of these two heavily used
modes.

# Modo de direccionamiento por desplazamiento
La mayor pregunta que surge del estilo de direccionamiento por desplazamiento es
el rango de desplazamiento usado. Basado en en el uso de varios tamaños de 
desplazamiento, se puede tomar una decisión de que tamaños soportar. 
Elegir el tamaño del campo de desplazamiento es importante porque afectan 
directamente al tamaño de la instrucción. 
La Figura A.8 muestra las medidas tomadas del acceso de información en una
arquitectura de carga-guardado usando un programa de benchmark del autor.
Podemos ver que los patrones de acceso y las ramas son diferentes; hay un poco
de ganancia combinandolas, a pesar de eso en la practica el tamaño inmediado 
es igual por simplicidad.
(Poner las imágenes en la presentación)

# Modo de direccionamiendo literal o inmediato
El direccionamineto inmediato puede ser usado en operaciones aritméticas, en
comparaciones (principalmente para las ramas) y en movimientos donde es 
constante el uso en el registro. El último caso ocurre cuando escribimos código
que tiende a ser pequeño pero en direccionamiento es constante que tiende a 
alargarlo. Para usar el direccionamiento inmediato es necesario saber si se 
necesita para soportar todas las operaciones o sólo para algunos casos.
La Figura A.9 muestra la frecuencia del direccionamiento inmediato para las
clases generales de enteros y punto flotante en el conjunto de instrucciones.
Otra medida importante a ver es el rango de los valores para los inmediatos.
Como los valores de desplazamiento, el tamaó de los valores inmediatos afectan
el tamaño de la instrucción. Como la Figura A.10 muestra, pequeños valores
inmediatos son más usado. Inmediado largo son usados de vez en cuando, sin 
embargo son más probables en los cálculos de direccionamiento.

# Resumen: Direccionamiento de memoria
Primero, debido a su popularidad, nosotros podrías esperar una nueva 
arquitectura que soporte al menos los siguientes modos de direccionamiento:
desplazamiento, inmediato y registro indirecto. En la figura A.7 muestra que
esos modos representan el 75%-99% de los direccionamientos usados en nuestras
medidas. Segundo, podríamos esperar que el tamaño del desplazamiento sea
al menos de 12 a 16 bits, visto en la figura A.8 sugiere que el tamaño que
podríamos capturar es del 75% al 99% de desplazamientos.
Tercero, podríamos esperar que el tamaño del campo inmediato sea al menos de 8 a
16 bits. Esta afirmación no es substancial en la figura a la que se hace 
referencia.
Con todo esto podemos pasar al tipo de tamaño de los operandos.

# Tipo y tamaño de los operandos.
¿Cómo se designa el tipo de un operando? Normalmente codificandolo en el código
de operación designado al tipo de un operando, este metodo se usa bastante.
Alternativamente la información puede ser etiquetada e interpretada por el 
hardware. Estas etiquetas especifican el tipo del operando y la operación es
elegida acordemente. Las computadoras con información etiquetada, sólo pueden 
ser encontradas en un museo.
Empecemos con las arquitecturas de desktop y servidores. Usualmente el tipo de 
un operando, entero, punto flotante de una simple precisión, carácter, etc, 
efectivamente da su tamaño. Los tipos comúnes de operandotes incluyen 
carácteres (8 bits), media palabra (16 bits), palabra (32 bits), punto flotante
de una simple precisión (también 1 palabras/32 bits) y de doble precisión 
(64 bits). Los enteros son universalmente representados por dos números binarios
complementados. Los caracteres usualmente en ASCII, pero Unicode de 16bit (usado
en JAVA) ha ganado más popularidad e internalización en computadoras. Hasta 
inicios de los 80's la mayoría de las computadoras que se fabricaban tenían su
propia representanción de punto flotante. Después de esa época las computadoras
han estándarizado el punto flotante con el IEEE estándar 754. 
(Se discute sobre el IEEE en el Apendice J).
Algunas arquitecturas  proveen operaciones en los carácteres de cadena, a pesar
de eso la mayoría de las operaciones estan limitadas porque tratan cada byte de
la cadena como un simple carácter. Las operaciones típicas en estas cadenas son
comparaciones y movimientos.
Para las aplicaciones de negocios, algunas arquitecturas soportan el formato 
decimal usualmente llamado: packed decimal o binary-coded decima, 4 bits son 
usados para codificar los valores del 0 al 9 y 2 digitos decimales son 
empaquetados en cada byte. Los cáracteres numericos en cadena usualmente son
llamados unpacked decimal, y operaciones llamadas packing and unpacking son 
usadas para convertirlos entre ellos.
Una de las razones para las que usamos operandos decimales es para obtener 
resultados exactamente iguales en números decimales, como algunas fracciones
decimales no tienen representación en binario.
Por ejemplo, 0.10 base 10 es una simple fracción en decimal, pero en binario
requiere un conjunto infinito de digitos repetidos: 0.0001100110011..._ base 2.
Es por esto que algunos calculos en decimal que podrían ser exactos en una 
computadora podrían ser inexactos, lo cuál podría causar problemas si hablamos
de transacciones financieras. (Ver Apendice J para ver acerca de la precisión
aritmetica).
Nuestro benchmark SPEC usa byte, caracter, media palabra (short integer), 
palabra (integer), double-word (long integer), y tipo flotante, en la figura 
A.11 muestra la distribución dinámica de los tamaños a los que la memoría hace
referencia para estos programas. La frecuencia del acceso a la diferente 
información nos ayuda a decidir qué tipo es más importante en eficiencia. 
¿Debería tener la computadora un acceso de 64-bit o deberíamos tomar dos ciclos
para que el acceso de una palabra doble sea satisfactorio? Como vimos al 
principio, el acceso de byte requiere una conexión de alineamiento: 
¿Qué tan importante es tratar a los bytes como primitivos? La figura A.11 usa
referencia de memoria para examinar el tipo de información a la que accesará.
En algunas arquitecturas, los objectos en los registros podrían accesar como 
bytes o medias palabras. Sin emabrgo, este acceso es bastante infrecuente, en
VAX, no cuenta más que el 12% de las referencias a los registros o apenas un 
6% de todo el acceso de los operandos en estos programas.

# Operaciones en el conjunto de instrucciones
Explicar todas las imagenes (A.12 y A.13) que vienen en la presentación.
Ponerlas en una hoja aparte y colocar sólo en la presentación las imágenes.

# Intrucciones para el control de flujo
Debido a que las medidas de las ramas y los saltos es bastante independiente de
otras mediciones y aplicaciones, nosotros examinaremos el uso de las 
instrucciones del control de flujo, que tienen algo en común con las operaciones
en las secciones previas.
No hay una términología consistente para las instrucciones que cambian el flujo 
de contro. En los 50's eran llamados transfers. Empezando los 60's el nombre
branch empezó a ser usado. Después, las computadoras introdujeron nombres 
adicionales. A lo largo de este libro usaremos jump/salto cuando el cambio en 
el control es incondicional y branch/rama cuando el cambio es condicional.
Podemos con esto distinguir 4 tipos de control de flujo:
* Conditional branches
* Jumps
* Procedure calls
* Procedure returns

Queremos saber entonces la frecuencia relativa que tienen estos eventos, como
cada uno es diferente, podrían usar diferentes instrucciones y podrían tener 
diferente comportamiento. La figura A.14 muestra la frecuencia de estos flujos 
de control para una computadora con arquitectura load-store corriendo en
nuestros benchmarks.

# Modos de direccionamiento para las instrucciones de control de flujo
El direccionamiento de destino en una instrucción de control de flujo siempre 
debe de ser especificada. Este destino es especificado explicitamente, en la
mayoría de los casos, en la instrucción, -el procedimiento de returno es la 
mayor excepción, ya que el objetivo de retorno no es conocido en tiempo de 
compilación. La forma más común de especificar el destino es suplir un 
desplazamiento que es añadido en el contador del programa (PC). Las 
instrucciones de control de flujo son llamadas PC-relative. Las ramas o saltos
de los PC-relative son ventajaos debido a que su objetivo es cercano a las 
instrucciones actuales y especificar la posición relativa requiere pocos bits.
Usar los accesos de PC-relative permite que el código corra de manera 
independiente en cualquier lado que sea cargado. Esta porpiedad es llamada 
position independence, puede eliminar cualquier trabajo cuando el programa esta
unido y es útil en porgramas que tienen unión dinámica durante su ejecución.
Para implementar retornos y saltos indirectos cuando el objetivo no es conocido 
en tiempo de compilación, otro método además del acceso de PC-relative es 
requerido. Debe de haber una manera de especificar el objetivo dinamicamente, 
así que debe cambiar en tiempo de ejecución. Este acceso dnámico debe ser simple
cómo llamar a un registro que contiene las direcciones objetivo, 
alternativamente el salto debería permitir que cualquier modo de 
direccionamiento para usarlo y suplir la dirección objetivo.
Estos saltos a registros indirectos son útiles para otras 4 caracteristicas:
* Estados case or switch encontrados en la mayoría de lenguajes (seleccionan 
una de varias alternativas).
* Virtual functions or methods en lenguajes orientados a objetos como C++ o Java
(los cuales permiten diferentes rútinas que pueden llamarse dependiendo el tipo 
del argumento).
* High-order functions or function pointers en lenguajes como C o C++ (lo cuales
permiten seguir funciones que son pasadas como argumentos, dando un poco de 
sabor a la programación orientada a objetos).
* Dynamically shared libraries (los cuales permiten una coleccion que puede ser 
cargada o vinculada en tiempo de ejcución sólo cuando es invocada por el 
programa en vez de ser cargada y vinculada estaticamente antes de que el 
programa sea ejecutado).

En los 4 casos la dirección de destino no es conocida en tiempo de compilación 
es por eso que usualmente es cargada en memoria dentro de un registro antes
de saltar indirectamente a un registro.
Como las ramas son generalmente usadas en los direccionamientos de PC-relative
para especificar sus objetivos, una pregunta importante que debemos hacernos
es qué tan lejos están los objetivos de la ramificación. Conocer está 
distribución de estos desplazamientos nos ayudará a elegir que rama compensa el
soporte y así saber cómo afectara el tamaño de la instrucción al codificarla.
La figura A.15 muestra la distribución del desplazamiento para las instrucciones
en las ramas de los PC-relative. Cerca del 75% de las ramas están en dirección
de avance. 
(Mostrar la imagen y explicar la gráfica).

# Opciones de las ramas condicionales
Desde que la mayoría de los cambios en el control de flujo son ramificaciones,
deicidir cómo especificar la rama conficional es importante. La figura A.16
muestra las 3 principales técnicas que son usadas a día de hoy, junto con sus
ventajas y desventajas. (Poner la imagen y explicarla)
Una de las propiedades más notables de la ramificación es el largo número de
comparación que se hacen en tests simples y un largo número de comparación con
cero. Por lo que, algunas arquitecturas eligen tratar estás comparaciones como
casos especiales, especificamente si una instrucción compare and branch esta 
siendo usada. La figura A.17 muestra la frecuencia de diferentes comparaciones 
usadas para ramificaciones condicionales.

# Opciones del procedimiento de invocación
Las llamadas a procedimientos y retornos incluyen transferencia de control y 
posiblemente algun estado de guardado; como minimo una dirección de retorno debe
ser guardada en algún lado, algunas veces en un registro especoal o en un GPR (
Registro de propósito general). Algunas arquitecturas antigüas requieren que el
compilador genere almacenamientos y cargas para cada registros que es guardado
y restaurado.
Hay dos convenciones básicas paara guardar registros: ya sea en el sitio que lo 
llamamos o dentro de un procedimiento que será llamado. Caller saving quiere 
decir que las llamadas al procedimiento debe guardar los registros que quiera 
preservar para el acceso después de la llamada y así la llamada no necesita 
preocuparse por los registros. Callee saving es lo opuesto: la llamada al 
procedimeinto debe guardarse en los requistros que quiere usar, dejando la
llamada irrastreable. Habrá veces donde caller saver debe ser usado porque 
los patrones de acceso para las variebles globales visibles están en dos
procedimientos diferentes. 
Por ejemplo, supongamos que tenemos un procedimiento llamado P1 que llama a un
procedimiento P2, y ambos procedimeintos manipulan la variable globlar x. Si P1
tiene alojada a x en su registro entonces debemos estar seguro de guardar a x en 
la locación conocida por P2 antes de llamar a P2. Una habilidad del compilador
es descubrir cuando un procedimiento llamado puede acceder a cantidades 
asignadas por los registros se complica por la posibilidad de una compilación
separada. Supongamos que P2 no toca a x pero puede llamar a otro procedimiento
digamos un P3 que tiene acceso a x, todavía P2 y P3 son compilados de manera 
separada. Debido a estas complicaciones, la mayoría de los compiladores deben
conservar caller save en cualquier variable que podrían acceder durante su 
llamada.
En los casos donde sea convención podremos usarlo, algunos programas estarán más
optimizados con calee save y alguno con caleer save. Como resultado, la mayoría
de sistemas reales hoy en día usas una combinación de ambos mecanismos. Esta 
convención es especificada en una interfaz binaria (ABI) que establece las 
reglas basicas de cuales registros deben ser caller saved y cuales deben ser
callee saved.

# Resumen: Instrucciones para el control de flujo
Las instrucciones de control de flujo son las mas frecuentes en ser ejecutadas.
Aunque hay varias opciones para las ramas condicionales, podemos esperar ramas
de direccionamiento en una nueva arquitecctura que sea capaz de saltar cientos 
de instrucciones ya sea rriba o abajo de su rama. Esto requiere que el 
desplazamiento de la rama de PC-relative sea al menos de 8bits. También 
podríamos para ver los registros indirectos y el direccionamiento PC-relative
para un salto de instrucciones que soporte retornos tan bien como otras 
caracteristicas en los sitemas actuales.

