Notas para el Proyecto Final
=========================

Proyecto: Máquina Virtual
----------------------

### Componentes del simulador
La MV tiene que tener los siguientes componentes:
1. Registros: 14 registros
2. Memoria Primaria: Simular la memoria primaria de la maquina virtual 
   (RAM en los equipos). _La memoria es un arreglo de bytes que almacenan datos_
3. Unidad Aritmética-Lógica y Unidad de Control: Decodificará y ejecutará las
   instrucciones dadas en la especificación de la máquina virtual.
4. Llamada al sistema: 2 tipos de _syscalls_: Imprimir y leer de consola.
5. Manejo de errores: En caso de que haya un fallo en la máquina o un error 
   fatal, detener la ejecución de manera controlada. Devolver un código de error
   y almacenar un volcado de memoria en un archivo.


### Especificaciones de la máquina virtual
1. _20 opcodes:_ 4 de aritmética entera, 4 de aritmética de punto flotante, 4 
   para operaciones de bits, 4 para operaciones de memoria, 3 operaciones de
   salto de instrucción y una para la instrucción de llamada al sistema.
2. _14 registros:_ 8 registros de propósito general, 2 registros de argumentos 
   para llamada al sistema, 1 registro de retorno de datos de llamada al 
   sistema, 1 registro de contador de programa, 1 registro de apuntador a la
   pila de memoria y 1 registro para apuntar a la dirección de regreso. 
3. _9 syscalls:_ 4 instrucciones para leer de la consola, 4 para escribir y 1
   para terminar el programa.
   
### Registros
La máquina constará de _14 registros de 32 bits._

    | Registros | Descripción                            |
    |-----------+----------------------------------------+
    | 0,...,7   | Registros de propósito general         |
    | 8,9       | Argumentos para llamada al sistema     |
    | 10        | Retorno de datos de llamada al sistema |
    | 11        | Dirección de retorno                   |
    | 12        | Contador del programa                  |
    | 13        | APuntador a la pila de memoria         |
    +----------------------------------------------------+
    
### Códigos de operación (opcodes)

    +-----------------------------------------------------------------------------+
    | Operación | Código (en hex) | Duración (ciclos) |  Descripción              |
    +-----------------------------------------------------------------------------+
    | add       | 0x0             | 3                 | Suma entera (con signo)   |
    +---------------------------------------------------------------------------------+
    | sub       | 0x1             | 4                 | Diferencia entera (con signo) |
    +---------------------------------------------------------------------------------+
    | mul       | 0x2             | 10                | Producto entero (con signo)   |
    +---------------------------------------------------------------------------------+
    | div       | 0x3             | 11                | Cociente entero (con signo)   |
    +---------------------------------------------------------------------------------+
    | fadd      | 0x4             | 4                 | Suma flotante                 |
    +---------------------------------------------------------------------------------+
    | fsub      | 0x5             | 5                 | Diferencia flotante           |
    +---------------------------------------------------------------------------------+
    | fmul      | 0x6             | 9                 | Producto flotante             |
    +---------------------------------------------------------------------------------+
    | fdiv      | 0x7             | 10                | Cociente flotante             |
    +---------------------------------------------------------------------------------+
    | and       | 0x8             | 1                 | Operador de bits AND          |
    +---------------------------------------------------------------------------------+
    | or        | 0x9             | 1                 | Operador de bits OR           |
    +---------------------------------------------------------------------------------+
    | xor       | 0xA             | 1                 | Operador de bits XOR          |
    +---------------------------------------------------------------------------------+
    | not       | 0xB             | 1                 | Operador de bits NOT          |
    +---------------------------------------------------------------------------------+
    | lb        | 0xC             | 500               | Cargar byte                   |
    +---------------------------------------------------------------------------------+
    | lw        | 0xD             | 1500              | Cargar palabra (4 bytes)      |
    +---------------------------------------------------------------------------------+
    | sb        | 0xE             | 700               | Guardar byte                  |
    +---------------------------------------------------------------------------------+
    | sw        | 0xF             | 2100              | Guardar palabra (4 bytes)     |
    +---------------------------------------------------------------------------------+
    | li        | 0x10            | 1500              | Cargar valor constante        |
    +---------------------------------------------------------------------------------+
    | b         | 0x11            | 1                 | Salto incondicional           |
    +---------------------------------------------------------------------------------+
    | beqz      | 0x12            | 4                 | Salto si es igual a cero      |
    +---------------------------------------------------------------------------------+
    | bltz      | 0x13            | 5                 | Salto si es menor a cero      |
    +---------------------------------------------------------------------------------+
    | sycall    | 0x14            | 50                | Llamada al sistema            |
    +---------------------------------------------------------------------------------+
    
    
### Llamadas al sistema (syscalls)

Sera igual que en _MIPS_: Cargar un código de llamada del sistema en un 
registro, 1 argumento en otro registro y se devolverá algún dato en el tercer
registro. 
La convención de entrada y salida de datos en las llamadas al sistema serán:
1. Registro para código de llamada: 8
2. Registro para pasar el argumento a la llamada: 9
3. Registro donde se reciben datos de la llamada: 10

Los _códigos de llamada al sistema_ serán los siguientes:
* Códigos para leer de la consola:
  
        +-------------------------------------------------------------------------------+
        |Código   |  Significado  |  Retorno (en el registro 10)                        |
        +---------+---------------+-----------------------------------------------------+
        |0x0      | Leer entero   | Entero leído                                        |
        +---------+---------------+-----------------------------------------------------+
        |0x1      | Leer carácter | Carácter leído                                      |
        +---------+---------------+-----------------------------------------------------+
        |0x2      | Leer flotante | Número flotante leído                               |
        +---------+---------------+-----------------------------------------------------+
        |0x3      | Leer cadena   | Número de caracteres leídos.                        |
        |         |               | Se debe especificar en el registro 9                |
        |         |               | la dirección de memoria donde se guardará la cadena |
        +---------+---------------+-----------------------------------------------------+

* Códigos para escribir en consola:

        +-----------------------------------------------------------------------------------+
        |Código   |  Significado      |  Argumento (en el registro 9)                       |
        +---------+-------------------+-----------------------------------------------------+
        |0x4      | Escribir entero   | Entero a escribir                                   |
        +---------+-------------------+-----------------------------------------------------+
        |0x5      | Escribir carácter | Carácter a escribir                                 |
        +---------+-------------------+-----------------------------------------------------+
        |0x6      | Escribir flotante | Número flotante a escribir                          |
        +---------+-------------------+-----------------------------------------------------+
        |0x7      | Escribir cadena   | Dirección de memoria donde empezará a imprimir.     |
        |         |                   | La cadena debe estar delimitada por el carácter     |
        |         |                   | nulo (al igual que C).                              |
        +---------+-------------------+-----------------------------------------------------+

* Salir del programa:

        +------------------------------------------------------------------------------------+
        |Código   |  Significado       |  Argumento (en el registro 9)                       |
        +---------+--------------------+-----------------------------------------------------+
        |0x8      | Salir del programa | No utilizado                                        |
        +---------+--------------------+-----------------------------------------------------+

### Códificación de instrucciones
Las instrucciones de la máquina virtual serán de 32 bits (4 bytes) y estarán 
codificadas de la siguiente manera:

      +---------+----+------+------+
      | 0       | 8  |  16  |  24  |
      +---------+----+------+------+
      | Op Code | Dr | Op1  |  Op2 |
      +---------+----+------+------+
      
* _Op Code_ representa el código de operación.
* _Dr_ representa el número de registro dónde se guardará el resultado.
* _Op1 y Op2_ representan los números de registro de los operandos.
* En el caso de las instrucciones _BEQZ_ y _BLTZ_ la dirección de los saltos 
  estarán en _Dr_ y el registro a evaluar será _Op2._
* Para las instrucciones que sólo tienen un operando (carga y almacenamientos de
  memoria, saltos de instrucciones y el operador NOT) sólo se toma en cuenta 
  como operando el valor almacenado en el campo _Op2_. 
* El caso de la operación _li_ es especial, ya que es una instruccion que tiene 
  un tamaño de 6 bytes y su decodificación es la siguiente:
  
        +------+-----+------+------+------+------+
        | 0    |  8  |  16  |  24  |  32  |  40  | 
        +------+-----+------+------+------+------+
        | 0x10 | Dr  |       Cons de 32 bits     |
        +------+-----+---------------------------+

_Esto complicará un poco la implementación de la máquina, sin embargo, no debe
ser un obstáculo muy complicado de superar._
        
### Pila de memoria
El _stack pointer_ (registro 14) siempre empezará apuntando al último byte de la
memoria primaria.

Los programas siempre se cargarán en los primeros bytes de la memoria y el 
apuntador se moverá del fin hacia el inicio.

### Códigos de error
En caso de ocurrir un error fatal, su máquina virtual deberá finalizar su 
ejecución devolviendo un código de error que especifica la falla ocurrida.

Deberá guardar un volcado de la memoria primaria en un archivo.

Deben devolver el control al sistema operativo devolviendo un _exit code_ como
aparece en la siguiente tabla:

      +-----------------+--------------------------------------------------+
      | Código de error |  Signficado                                      |
      +-----------------+--------------------------------------------------+
      |        1        |  División entre cero                             |
      +-----------------+--------------------------------------------------+
      |        2        |  Dirección de memoria inválida                   |
      +-----------------+--------------------------------------------------+
      |        3        |  Memoria agotada                                 |
      +-----------------+--------------------------------------------------+
      |        4        |  Número de registro inválido                     |
      +-----------------+--------------------------------------------------+
      |        5        |  Código de operación inválido                    |
      +-----------------+--------------------------------------------------+
      |        6        |  Código de llamada al sistema inválido           |
      +-----------------+--------------------------------------------------+

### Requerimientos del simulador
El simulador además de ejecutar programas escritos en lenguaje máquina, deberá
de implementar las siguientes características.

### Argumentos de línea de comando
La invocación de la máquina virtual deberá ser de la siguiente forma:

    $ ./myvm -m 65536 helloworld.bin
 
Donde el argúmento `-m` representa el tamaño de la memoria principal (medida en
bytes), y el archivo `helloworld.bin` representa un programa escrito en el 
lenguaje máquina que interpretará su simulador.

### Ejecución
En caso de que la ejecución del programa sea satisfactoria, su simulador debe 
imprimir al final un número entero positivo que representa el tiempo de 
ejecución del programa medido en ciclos de reloj.

Ejemplo:

      $ ./myvm -m 1048578 helloworld.bin
      Hello World!
      6950

Para esto, _debemos llevar la cuenta de los ciclos de reloj de cada instrucción
que ejecuta el programa_ e _imprimir la suma al final de la ejecución._ 
La duración de ciclos de reloj de cada instrucción está especificada en la 
tabla de operaciones que deben implementar.

Si el programa produce un error fatal en la máquina virtual, la ejecución se 
deberá finalizar inmediatamente y deberá guardar un volcado de la memoria en
el archivo `dumpfile.bin`. _NO_ debemos imprimir el número de ciclos de reloj en
este caso, _debemos salir inmediatamente_.

### Palabras reservadas
### Instrucciones
Todas las instrucciones se delimitan por el carácter de nueva línea.

Las siguientes palabras reservadas representan las instrucciones de la máquina
virtual: _add, sub, mul, div, fadd, fsub, fmul, fdiv, or, and, xor, not, lb, sb,
lw, sw, beqz, bltz, b, li, syscall_

### Instrucciones adiccionales
Para facilitar el trabajo del programador, el compilador ofrece 1 instrucción
adicional:
* _mov (move)_: Para asignar el valor de un registro a otro.

### Registros
Los identificadores de registro, al  igual que en SPIM utlizarán el símbolo `$`.
No se utilizará el número de registro como en el código máquina, sino un nombre
más descriptivo que se muestra en la tabla a continuación:


        +-----------------------------------------------------------------------------------------------+
        |Nombre        |  Número de registro      |  Descripción                                        |
        +--------------+--------------------------+-----------------------------------------------------+
        |$r0,...,$r7   | 0,...,7                  | Registros de propósito general                      |
        +--------------+--------------------------+-----------------------------------------------------+
        |$a0,$a1       | 8,9                      | Registros de argumentos en llamada al sistema       |
        +--------------+--------------------------+-----------------------------------------------------+
        |$s0           | 10                       | Registro de retorno para llamada al sistema         |
        +--------------+--------------------------+-----------------------------------------------------+
        |$ra           | 11                       | Registro para almacenar direcciones de retorno      |
        +--------------+--------------------------+-----------------------------------------------------+
        |$pc           | 12                       | Contador del programa                               |
        +--------------+--------------------------+-----------------------------------------------------+
        |$sp           | 13                       | Apuntador al tope de la pila de memoria             |
        +--------------+--------------------------+-----------------------------------------------------+


### Definiciones

Los _macros_ para la definición de datos son:
* `.text`
  Delimita el inicio del código de su programa.
* `.ascii`
  Sirve para representar arreglos de bytes, si vamos a reservar un arreglo de 
  enteros debemos multiplicar el tamaño del arreglo por 4, la sintaxis es:
  `.ascii ID INT`
  Donde _ID_ representa un identificador del arreglo e _INT_ una literal entera 
  que representa el tamaó en bytes.
* `.asciiz`
  Representa cadenas de caracteres. A las cadenas definidas con _.asciiz_ se les
  agrega automáticamente el carácter nulo (\0) al final. Su sintaxis es:
  `.asciiz STR ID`
  Donde _STR_ representa una literal de cadena e _ID_ representa un 
  identificador para la cadena. Las literales de la cadena son del estilo de C.
  Se soportan las secuncias de control `\[bnft]` para _Backspace, Newline, Line
  Feed y Tab_.

El delimitador `.text` es obligatorio en cualquier programa para especificar en
dònde inicia el código del mismo. Todos los demás macros son opcionales. _Todas
las definiciones de variables deberán hacerse antes del token .text_.

### Etiquetas
Para definir subrutinas se puede hacer uso de etiquetado al igual que en SPIM, 
utilizando el carácter `:` para especificar una subrutina.
_Todo programa debe tener una etiqueta main la cual representa el punto de 
arranque del programa._

### Funciones no soportadas del intérprete SPIM
* No se soporta el manejo de direcciones a la memoria con la sintaxis _Offset_
  ($registro) por lo cual, toda la aritmética de direcciones la deberán de hacer
  manual.
* No se soporta el uso del _Frame pointer._
* No se soportan las instrucciones _jump_ y _link_. Deberán guardar la pista de 
  la dirección de retorno en la pila de memoria de manera manual en caso de 
  implementar funciones.
* No se permmite usar identificadores para ninguna función de instrucción que no
  sea _li_. Por lo tanto, siempre que se quiera un valor de variable 
  (o identificador) se debe primero usar _li_ para cargar un registro.

### Paquete de pruebas.
En `/test` se encuentran los archivos de prueba, tanto escritos en ensamblador 
con terminación `.s` y los que tiene el código máquina generado de dichos 
programas con terminación `.bin`.

Los programas de prueba llamados _err1,err2,err3,err6_ deben detenerse con el 
tipo de error correspondiente.

Los programas _branch, flt, int, log y mem_ tienen un archivo con terminación 
`.res`, dentro de este archivo esta lo que el simulador debe imprimir en la 
consola para que este sea correcto.

El programa _syscall_ pide al usuario los datos, mismo que deben ser impresos
inmediatamente después para que esta prueba sea correcta.
