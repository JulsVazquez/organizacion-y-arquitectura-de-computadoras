# Notas de la lectura Fundamentals of Quiantitative Desing and Analysis Part 1

A pesar de las las mejoras tecnológicas que han sido constantes, el progreso de 
nuevas arquitecturas ha sido menos constante.
Durante los primeros 25 años de las computadoras, ambas tecnologicas mejoraron 
cerca del 25% por año.

A finales de los 70's se vio el nacimiento de los microprocesadores.
Con eso las mejoras crecieron cerca del 35% por año.

Este crecimiento junto con la producción en masa de microprocesadores 
permitieron un creciemiento para las computadoras para negocios basadas en 
microprocesadores.

Dos cambios significantes en el mercado de las computadoras hicieron que estás 
tuvieran un triunfo comercial. Primero la eliminación virtual del lenguaje 
ensamblador redujo la necesidad de la compatibilidad objecto-código.
Segundo, la creación de un estándar en el sistema conocido como UNIX.

Estos cambios hicieron posible el desarrollo satisfactorio de una nueva 
arquitectura de instrucciones simples llamada **RISC** (Reduced Intruction Set
Computer) a principios de los 80's.

La arquitectura RISC basaba su atención en dos técnicas, la explotación del
paralelismo de instrucción (instruction-level parallelism), lo hizo a traves de
canalización y después a traves de múltiples problemas de instrucciones, y el 
uso de la cachés (inicialmente como una simple forma y luego usada de manera más
sofisticada para optimizar y organizar).

Las computadoras basadas en RISC incrementaron la barra de rendimiento 
priorizando la arquitectur de mantener o desaparecer.
El Digital Equipment Vac no pudo competir con esto y fue remplazada por la 
arquitectura RISC. Intel en cambio se mantuvo a la altura del desafio, primero
por la traducción de la instrucción 80x86 dentro de las instrucciones internas 
de RISC. 

Como la cantidad de trasistores de disparó a finales de los 90's, el hardware
sobrepaso la traducción de la más compleja arquitectura x86. 
En los celulares el costo de poder en la arquitectura x86 hizo que una nueva 
arquitectura predominará la cual fue ARM.

Las mejoras en el costo de operación permitió una nueva clase computadoras 
llamadas: Las computadoras personales, las cuales nacieron en los 80's con la 
disponibilidad del microprocesador.

Para mantener la productividad y acercarnos más a la brecha de rendimiento, los
interpretes JIT (Just-In-Time) y el complilado rastreable 
(trace-based compiling) están remplazando alos metodos den compilación 
tradicional.

El despliegue de Software también está cambiando con el SaaS (Software as a
Service) usado en todo el Internet remplazando al software que debe ser 
instalado y ejecutado en una computadora local.

Y si esto no fuera poco, también están cambiando la naturaleza de las apps.
Discursos, sonidos, imagenes y video están creciendo de manera significativa,
como una respuesta inmediata para la experiencia de usuario. Un ejemplo de esto
es Google Translate (Traductor de Google). Esta app te permite escanear con tu 
cámara un texto o una foto y traducirla en el idioma que quieras.
También puedes hablar y lo que digas será traducido como output en otro idioma.
Puede traducir texto en 90 idiomas y voz en 15.

En 1974 Robert Dennard observó que el poder de densidad era constante env una 
área especifica del silicio incluso incrementando el número de transistores 
debido a las dimensiones de cada transistor. 
Notablemente los transistores podrían ir más rápido pero con menor consumo de
energía. 

La escala de Dennard terminó alrededor del 2004, debido a la corriente y el 
voltaje no podría mantenerse dependiente de los circuitos integrados.
Este cambio forzó a la industria a usar multi-procesadores más eficientes
con más de un núcleo.
En 2004 Intel canceló sus proyectos de alto desarrollo de procesadores de un
núcleo y declarando su camino a los multiprocesadores, que sería la vía más 
rápida y mejor a resolver el problema.

La ley de Amdahl prescribe que los limites practicos del número util de núcleos
de un chip. Si el 10% de la tarea es en serie, entonces el máximo rendimiento 
que tendremos con el paralelismo será de 10, no importa cuántos núcleos pongamos
en nuestro chip.

* Los transitores no mejoran muchos debido a lo lento de la ley de Moore y el 
final de la escala de Dinnard.
* El poder inmutable de los microprocesadores
* El remplazo de un sólo procesador que consume mucha energía con varios que 
consumen energia eficiente.
* Las limitaciones del multiproceso dado por la ley de Amdahl.

# Clases de Computadoras

Los cambios que se han producido en la computación o en el computar e incluso
en las apps que existen han hecho que la computación se vea de otra forma los 
cuales permiten cinco tipos de mercados para la computación caracterizadas por
diferentes aplicaciones, requerimientos y tecnologías.

# Internet of Things / Embedded Computers

Los embedded computers o computadores integrados podemos encontrarlos hoy en día
en cualquier maquina: microondas, lavadoras, impresoras, automoviles, lamparas,
etc.

El Internet de las cosas (IoT) se refiere a los computadores integrados que 
están conectados al Internet, por WIFI. 

Según las proyecciones, para el 2020 se prevee que el número de dispositivos con
IoT será de entre 20 y 50 mil millones.

# Personal Mobile Device

Este termino es acuñado para los dispositivos que cuentan con conexión WIFI y 
son dispositivos multimendia con interfaz de usuario, como telefonos, tablets, 
etc.
El enfásis tanto ecónomico como de coste de energía es dado por el uso de
baterias.

Los procesos de un PMD pueden ser considerados como computadores integrados, 
pero se mantienen en una categoria separada porque los PMD puede correr
de manera externa software y comparten varias caracteristicas con los 
computadores de escritorio. Otras computadoras integradas son más limitadas en 
el hardware y sofisticadas en el software. Usamos la habilidad de correr 
software de terceros para dividir la linea entre los computadores integrados
y los no computadores integrados.

Otra caracteristica de las aplicaciones de las PMD es que necesitan minimizar 
memoria y los procesos deben aceptar un siguiente de manera rápida.
También necesitan ocupar la energía de manera eficiente.

# Desktop Computing

La primera y de el posible mercado más grande en términos de dolares.
El precio rendimiento de estos sistemas es lo que más les importa a los 
usuarios en el mercado y también a los diseñadores de computadoras. Como 
resultado la novedad de los grandes rendimientos en microprocesadores que 
reducen el costo raramente aparecieron en los primeros sistemas de computadoras
de mesa.

# Servers

Muchos de los servidores se convirtieron en la espina dorsal de las grandes
empresas de computadoras, remplazando al tradicional ordenador central.

Para los servidores las difierentes caracteristicas son importantes. Primero
la habilidad de ser criticas, consideremos un servidor de máquinas ATM para 
bancos. Las fallas de estos sistemas podrían causar escenarios catastroficos
si sólo fueran un computador. Además de que estos deben operar 24/7.

# Clusters/Warehouse-Scale Computers
El crecimiento del SaaS (Software as a System) para las aplicaciones de 
búsqueda, redes sociales, transmisión de video, videojuegos enlínea.
Han creado una nueva terminología de computadoras llamada clusters.

Los clusters son colecciones de computadoras conectadas a una red local.
Cada nodo corre con su propio sistema operativo y cada nodo se comunica con
su propio protocolo.

El precio-rendimiento y energia es importante para estos sistemas. La 
amortización anual de estos sistemas llega a los $40 millones.

# Tendencias en la tecnología.
El plan paara la evolución de una computadora es que el diseñador debe tener 
cuidado sobre los rápidos cambios en implementación. Hay 5 tecnologías las 
cuales son:
* Tecnología en circuitos lógicos integrados
La densidad de transistores fue creciendo cerca de 35% por año, cuadruplicandose
cerca de 4 años. El incremento estuvo en un rango del 10% al 20% por año.
Esto combinado con crecimiento de chips que fue cerca de 40%-44% por año, 
doblandose cada 18-24 meses. Esta tendencia fue conocida por la Ley de Moore.
Está ley ya no se aplica ya que fue decreciendo.
El número de dispositovs por chip fue creciendo pero decreció el rango.
* Semiconductores de DRAM
El crecimeinto de la DRAM también bajó.
Los 8 gigabits DRAM fue envíados en 2014, pero los 16 gigabits DRAM fue hasta el 
2019 y parece que no hbará 32gigabits de DRAm, estas tecnologías tendrán un tope
* Semiconductores de memoria
Esta no volatil memoria es el estándar en almacenamiento de los PMDs y 
rapidamente crecio en capacidad. En tiempos recientes ha crecido cerca de 
50%-60% por año, doblandose cada 2 años. 
* Tecnología de discos magneticos
Esta tecnologia crecio a inicios de los 90's cerca del 30% por año y llegó a
60% por año, pero incrementó al 100% en 1996, entre 2004 y 2011 bajó cerca del
40% por año doblando esta cantidad cada 2 años. Y recientemente sólo hay un 
incremento de 5% por año.
Seagate anunció que limitaría su producción en 2018.
HAMR es la última oportunidad para continuar el mejoramiento de la densidad de
los discos duros. Ya que las unidades flash son más baratas por bit.
* Tecnologías de conexión
Esta depende del mejoramiento y el camvio hacia los sistemas de transmisión.

Este rapido cambio en las tecnologías ha afectado el diseño de las computadoras.
Ya que su tiempo de vida es alrededor de 3-5 años antes de ser desplazados por
una nueva tecnología.
El impacto que se ha tenido con esto es el cambio a la tecnología de los 
multiprocesadores con multiples núcleos.

# Tendecías de mejoramiento: Bandwidth Over Latency
El bandwith o thorughput es el número total del trabajos hechos medido por un 
tiempo. Esto significa el número de megabytes transferidos por segundo en un 
disco. A diferencia de la latencia o el tiempo de respuesta que es el tiempo 
entre el inicio de una tarea. Esto se cuenta en milisegundos en el acceso al 
disco.
El mejoramiento es el principal diferenciador para los microprocesadores
y la conexión que ha tenido una ganancia en: 32,000x40,000 x in bandwidth y
50x90 x latencia. LA capacidad en general es más importante que el mejoramiento
de la memoria y los discos. 
Una simple regla para ver esto es que el ancho de banda crece al menos el 
cuadrado de lo que mejora la latencia.

# Escalado entre el mejoramiento de transistores y cables
Los circuitos integrados caracterizados por el feature size, el cual es el 
minimo tamaño de un transistor por un cable en dimesión. Estas caracteristicas
han decrecido desde las 10micras en 1971 hasta las 0.016micras en 2017.
Desde el conteo de transistores por milimetro cuadrado del silicio determina
la superficie de los transistores y la densidad de ellos ha crecido 
cuadraticamente con una disminución lineal.

El encogimiento vertical requiere una reducción en el voltaje para mantener
un correcto funcionamiento de los transistores. Esto combinado con los 
factores de escala permiten que la complejidad entre el mejoramiento de los
transistores y el tamaño. En el pasado las aproximaciones del mejoramiento de 
los transistores era lineal con un decrecimiento en tamaño.

# Tendencias en poder y energía de los circuitos integrados.
Hoy en día el mayor problema al diseñar es la energía. Debemos distribuir poder
en cada chip y los chips modernos tienen cientos de pines con multiples 
interconexiones para poder lograrlo. Además de poder disipar el calor.

# Poder y energía
Al diseñar debemos tener 3 puntos esenciales. Primero es saber el número máximo
de poder que necesita siempre un proceso. Conociendo esto podemos trabajar con 
el correcto funcionamiento.
Por ejemplo si le proporcionamos un voltaje mayor al procesador del que la 
máquina puede proveer el resultado es una caída de voltage el cual puede causar
un malfuncionamiento en el dispositivo. 

Segundo es saber cuándo poder podemos suministrar, está métrica es llamada
diseño de poder térmico, porque determina cuánto enfriamiento requerimos.
No medir correctamente esto puede provocar que el dispositivo falle o tenga un
daño severo. Primero debemos aprovechar la unión del limite de temperatura y el 
clock rate, reduciendo el poder. Si esto no sucede entonces se activa la segunda
fase que es bajar el poder del chip y así bajar su temperatura.

Tercero, debemos considerar la eficiencia de energía. Generalmente es mejor 
medir la energía para una tarea especifica. En particular la energía de una 
tarea realizada es igual al tiempo de ejecución de un trabajo.
Así si queremos saber la eficiencia entre dos procesadores debemos comparar 
la energía de una tarea ejecutandose.
Por ejemplo si el procesador A tiene un 20% de consumo energetico que el 
procesador B pero A ejecuta la tarea en sólo el 70% del tiempo necesitado por
B entonces el consumo energetico sera 1.2x0.7=0.84 que es claramente mejor.

Para arreglar una tarea es mejor disminuir el tiempo del reloj el cual 
disminuye su poder pero no su energía.

Los precios de la materia prima para las computadoras a bajado a lo largo del
tiempo debido a la competitividad.

# Notas de la lectura Fundamentals of Quiantitative Desing and Analysis Part 2

# Dependability
Los circuitos integrados a lo largo de su historia han sido uno de los 
componentes de confianza de las computadoras. pero son bastante delicados y en 
ocasiones pueden existir fallas en los canales de comunicación. Los arquitectos
de computadoras deben de tener en cuenta esto y poder encontrar soluciones a 
estos problemas.

Las computadors son diseñadas y construidas en diferentes capas de abstracción.
Podemos decender de manera recursiva para ver por completo estos subsistemas y
qué es lo que los componen. 
Algunas fallas comunes que se tienen es la pérdida de poder que puede ser 
limitada por un sólo componente en un módulo. Así que considerar este error en
el módulo a este nivel podría ser acarrear un problema al módulo en un nivel
superior. Esta distinción es de mucha ayuda al tratar de encontrar maneras de
como construir computadoras de confianza.

Una dificil pregunta es saber cuándo un sistema esta operando de manera 
apropiada. Y este punto se vuelve concreto con la popularidad de los servicios
de internet. 

Las infraestructuras nos proveen un servicio llamado SLA (service level 
agreements) o SLO (service level objectives) que garantizan que las conexiones
o el servicio de energía es de confianza (o sea que sirve).

Los sistemas alternan entre dos estados de servicio con respecto a la SLA:
* Service accomplishment: donde el servicio es entregado especificamente
* Service interruption: donde la entrega del servicio es diferente del SLA

La transición entre estos dos estados causa fallas (del estado 1 al estado 2) o
restauraciones (del estado 2 al estado 1). Contando las transiciones permite 
dos maneras de confianza;
* Module reliability: Es una medida de que el servicio se ha entragado de manera
continua desde en un punto de referencia inicial. El MTTF (mean time to failure)
también es una medida de fiabilidad, se reporta en fallas por mil millon de 
horas en operación o el FIT (failures in time). Así una MTTF de 1,000,000 de 
horas es equivalente a 10^9/10^6 o 1000FIT. Que el servicio se ha detenido es 
una medida para MTTR (mean time to repaire) o MTBF (mean time between failures)
que es la suma de los MTTF+MTTR.

* Module availability: Es una medida de que el servicio se completo respecto a 
una alternación entre dos estados de completez e interrupción. Para los 
sistemas no redundantes el modulo se habilita como:
Module availability = MTTF/(MTTF+MTTR)

Notemos que la fidelidad y la diponibilidad son metricas cuantificables los 
cuales son sinonimos de fidelidad. Con estas definiciones podemos estimar
la fidelidad de un sistema cuantificale si queremos asumir hacer de su
fidelidad en los componentes y sus fallas de manera independiente.

# Medición, reporte y resumen de rendimiento
Desafortunadamente el tiempo no siempre es una metrica en la cuál podemos
comparar siempre a los computadores. En nuestra posición la única medida
fiable y consistente del rendiemiento es el de programas reales.
Incluso en cada ejecución podemos definir diferentes maneras en las cuales 
poder contar. La mas común es llamada wall-clock time, response time o
elapsed time, que significa la latencia en la que una tarea se completa, esto
incluye acceso al almacenamiento, acceso a la memoria, actividades de entrada y
salida, etc. Con la multiprogramación, el procesador trabaja en otro programa
mientras espera una entrada/salidda y no necesariamente minimiza el tiempo de un
programa. Así que necesitamos considerar esta actividad.

CPU time reconoce la distinción y nos dice el tiempo en el que el procesador
se encuentra computando, no incluye el tiempo de espera de la entrada/salida de 
otros programas. Los usuarios que constantemente corren el mismo programa
podrían ser perfectos candidatos para evaluar una nueva computadora. Para
evaluar un nuevo sistema estos usuarios deben comparar el timepo de 
ejecución al correrlo en otra computadora. Una de las cosas que podemos 
aprovechar para saber cuándo una computadora es mejor con otra es en los 
programas benchmark que son programas que establecen el rendimiento relativo de 
las computadoras.

# Benchmarks

La mejor opción para medir el rendimiento en una aplicación real como por 
ejemplo Google Translate. Pero existen problemas al querer ejecutar programas 
que son muchos más simples que una aplicación real pueden provocar problemas de
rendimiento. Los ejemplos incluyen:
* Kernels: Que son pequeños y piezas fundamentales en aplicaciones reales
* Programas de juguete: Que son programas de 100 lineas asignados a 
programadores principantes como implementar Quicksort
* Synthetic benchmarks: Que son falsos programas creados para alcanzar una meta 
o comportamiento en aplicaciones reales como Dhrystone.

Estos tres son desacreditados hoy en día porque el compilador o el arquitecto 
pueden conspirar para hacer que una computadora parezca más rápida en estos
programas stand-in que en aplicaciones reales.

Para restringir este proceso e incrementar los resultados los desarrolladores de
benchmarks requieren que el vendedor use un compilador o una serie de banderas 
para todos los programas en el mismo lenguaje (C o C++ por ejemplo).
Hay tres formas de aprovechar esto:
* No se permiten las modificaciones del código fuente
* Las modificaciones son casi imposibles por la cantidad de líneas y su 
complejidad.
* Se permiten modificaciones siempre y cuando la versión alterada produzca la 
misma salida.

# Resultados de rendimiento
El principio de los reportes de rendimiento debe de ser reproducible.
Un reporte de benchmark de SPEC requiere una extensa descripción de la 
computadora y el las banderas del compilador. También como la publicación de 
ambos y el ajuste de lineas y descripción de parametros. 
Un reprote de SPEC contiene el tiempo de rendimiento actual junto con una tabla
y gráfica.
Un reporte TPC benchmark es aún más completo, porque debe de incluir los 
resultados de la audición del benchmark y el costo de la información. Estos 
reportes son por excelencia los recursos para encontrar los costos reales 
computacionales, desde su manofactura competente y su alto rendimiento y costo
rendimiento.

# Resumiendo los resultados de rendimiento
En la practica del diseño de computadoras, uno debe de evaluar varias elecciones
de diseño para obtener datos cuantitativos acerca del beneficio más alla de la 
que entregan los benchmarks.
De igual manera los consumidores deben de tratar de elegir un rendimiento 
medido por benchmarks que idealmente seran similares para los usuarios de las
aplicaciones. En ambos casos es util tener medidas de las suites de benchmarks 
para que el rendimiento de importantes aplicaciones sea similar o igual a uno o
más benchmarks.
En el mejor de los casos la suite se parece estadisticamente a un caso válido
de las aplicaciones en espacio, pero muchas veces requieren más de un 
benchmark.

Una vez gemos elegido el benchmark apropiado necesitamso esperar para los 
resultados y es ahí cuando empezaremos a resumir los resultados de rendimiento
en un único número. Una aproximación podría ser el resultado de comparar el 
número en tiempo de ejecución del programa. Una alternativa a esto podría ser 
añadir un factor constante para cada benchmark que será usado para cada una
de las pruebas.

Después de hacer esto podríamos normalizar los tiempos de ejecución de un 
programa dividiendo el tiempo de referencia de la computadora con el tiempo
que le tomo llegar a este. Algo similar hace SPEC llamado SPECRatio.

# Tomar ventaja del paralelismo
Usar paralelismo es uno de los métodos más importantes para verificar el 
rendimiento.
Ser capaz de expandir la memoria, el número de procesos y almacenamiento de 
dispositivos es llamado escalabilidad (scalability). 
SPECSFS toma el nivel de solicitud en el paralelismo para hacer varios procesos
TPC-C usa nivel de hilos para el paralelismo para hacer más rápido el proceso 
de las colas de los datos.
El pipelining es sobreponer instrucciones de ejecución oara reducir el tiempo
total que le toma a una secuencia de instrucciones en ser completada.

# Principio de localidad
Los programas tienden a reusar información e instrucciones que ya han hecho
recientemente. Una manera de verlo es que un programa gasta el 90% en su 
ejecución y sólo 10% del codigo. El principio de localidad también se puede 
aplicar al acceso de información.
Dos diferentes tipos de localidad han sido observados. Localidad temporal, 
donde los estados que hemos accedido son similares a los que accederemos.
Localidad espacial: Los cuales sus direcciones estan tan cerca la una de la otra
que pueden ser referenciados en un tiempo muy corto por ambas.

# La ley de Amdahl
La ley afirma que la mejora del rendimiento es ganada usando algún modo rápido
de ejecución que es limitado por una fracción del tiempo en el que el modo 
rápido puede ser usado.
Define el speedup que podemos ganar usando una característica en particular.

El Speedup nos dice que tan rápido una tarea puede correr usando una computadora
con mejora contra la original. Usa una forma rápida para encontrar la speedup
de alguna mejora que depende de dos factores:
* La fracción del tiempo en computo en la computadora original que puede ser
convertido en una ventaja de mejoramiento
* El mejoramiento ganado en modo de ejecución que nos dice que tan rápudo la 
tarea podría correr si usamos una mejora en todo el programa.
