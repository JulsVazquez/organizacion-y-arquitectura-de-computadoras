# Ejercicio 01 Preguntas de la Lectura

# Preguntas Lectura 01 Parte 1
  
* ¿En qué se basa la arquitectura RISC?
  - R: Basa su atención en dos tecnicas, la explotación del paralelismo de 
  instrucción a traves de la canalización y resolviendo múltiples problemas de
  instrucciones y el uso de la cache para optimizar y organizar de manera más 
  simple.
  
* *¿Cómo ha sido el crecimiento del número de transistores por año?
  - R: A finales de los 70's nacieron los microprocesadores dando una mejora cerca
  del 35% por año, además de que iban creciendo en número de transistores de 
  manera exponencial, pero esto fue disminuyendo (el número de transistores) 
  ya que se empezó a usar la arquitectura de multiprocesador y multinucleo

* ¿Cuál fue la contribución de Robert Dennard en los transistores en el año 1974?
  - R: Observó que el poder de densidad era constante en un área especifica del 
  silicio incluso incrementando el número de transistores debido a las dimensiones
  de cada uno de estos. Notablemente los transistores podrían ir más rápido pero 
  con menor número consumo de energía
  
* ¿Qué dice la escala de Dennard y cuándo dejó de usarse?
  - R: Establecio que la medida de los transistores reducen su tamaño físico, la 
  densidad de potencia se mantenía constante y este uso de energía se mantenía
  en proporción con el área del chip. Se dejó de usar alrededor del 2004 con la
  entrada de los multiprocesadores
  
* Describe la Ley de Amdahl.
  - R: Prescribe que los limites prácticos del número de núcleos de un chip. Si el 10%
de la tarea es en serie, entonces el rendimiento máximo con el paralelismo sería
del 10%, no importa cuántos núcleos pongamos en nuestro chip

* ¿Cuáles son las 5 clases de computadoras?
  - Internet of Things
  - Personal Mobile Device
  - Desktop Computing
  - Servers
  - Clusters

* ¿Cuáles son las 5 tendencías de tecnología en las computadoras?
  - Tecnología en circuitos lógicos integrados
  - Semiconductores de DRAM
  - Semiconductores de memorias flash
  - Tecnología de dicos magnéticos
  - Tecnologías de conexión

* ¿Para que sirve bajar la velocidad del reloj en un procesador?
  - Para disminuir el poder pero no el consumo de energía y así liberar menos calor

* ¿Qué es mejor, un procesador con menor consumo energético o uno que ejecuta la 
tarea más rápido pero con mayor consumo? ¿Qué sería lo ideal?
    - R: Un procesador que ejecuta la tarea más rapido pero con mayor consumo. Lo ideal
    es que hiciera la tarea más rápido y con menor consumo.

* ¿Menciona una de las razones por las que han bajado los costes de producción en las computadoras?
    - R: Debido a la competitividad del mercado y de los diferentes fabricantes que existen para
    esto, así puede haber mejores precios. Otra razón es que se han abaratado los costes de 
    producción como el de las materías primas.

# Preguntas Lectura 1 Parte 2

* Los sistemas alternan entre dos estados de servicio con respecto a la SLA. ¿cuáles son?
  - R: 
       * Service accomplishment
       * Service interruption.
* ¿Qué diferencia un reporte SPEC de un reporte TPC en un benchmark?
  - R: Un reporte SPEC contiene el tiempo del rendimiento actual junto con una tabla y una gráfica.
       Un reporte TPC es aún más completo porque debe de incluir los resultados de audición y el costo
       en la información.
* ¿Por qué usar paralelismo es uno de los métodos más importantes para verificar el rendimiento?
  - R: Porque podemos expandir la memoria, el numero de procesos y el almacenamiento de los dipositivos
* ¿Cuáles son los dos principios de localidad que se han observado?
  - R: 
      * Localidad temporal: Donde los estados que hemos accedido son similares a los que accederemos en
      un futuro.
      * Localidad espacial: Los cuales sus direcciones en memoria están tan cerca la una de la otra que
      podemos acceder a ellos en un tiempo muy corto.
* ¿Qué es el speedup?
  - R: Una medida que nos dice que tan rápido una tarea puede correr usando una computadora sin mejora
  con una que tiene una mejora.
  
  
