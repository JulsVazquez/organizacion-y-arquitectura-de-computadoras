# Práctica 03
# 2. Escribir un programa que reciba una cadena e imprima la reversa de ésta.
.data
	mensajeGlobal: .asciiz "Escribe tu cadena y te devolveré la longitud:"
	entrada: .space 32
	entradaReversa: .space 32
.text

	main:
	li $v0, 4
	la $a0, mensajeGlobal
	syscall
	
	li $v0, 8
	la $a0, entrada
	li $a1, 32
	syscall
	
	addi $t1, $zero, 0

	loop:
		lb $t2, entrada($t1)
		beqz $t2, exit
		addi $t1, $t1, 1 #i++
	j loop
	
	exit:

	addi $t1, $t1, -2
	addi $t3, $zero, 0
	
	loop2:
		lb $t2, entrada($t1)
		sb $t2, entradaReversa($t3)
		beqz $t2, exit2
		addi $t1, $t1, -1
		addi $t3, $t3, 1		
	j loop2
	
	exit2:
	li $v0, 4
	la $a0, entradaReversa
	syscall

			
	# Esta línea termina el programa
	li $v0, 10
	syscall
