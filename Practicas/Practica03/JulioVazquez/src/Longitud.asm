# Práctica 03
# 1. Escribir un programa que lea una cadena de consola e imprima la longitud 
#    de ésta.
.data
	mensajeGlobal: .asciiz "Escribe tu cadena y te devolveré la longitud:"
	entrada: .space 32
.text

	main:
	li $v0, 4
	la $a0, mensajeGlobal
	syscall
	
	li $v0, 8
	la $a0, entrada
	li $a1, 32
	syscall
	
	# Contador
	addi $t1, $zero, 0
	
	loop:
		lb $t2, entrada($t1)
		beqz $t2, exit
		addi $t1, $t1, 1 #i++
	j loop
	
	exit:
	sub $t1, $t1, 1
	li $v0, 1
	move $a0, $t1
	syscall
	
	# Esta línea termina el programa
	li $v0, 10
	syscall
