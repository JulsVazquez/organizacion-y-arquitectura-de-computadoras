Práctica 02
===========

Organización y Arquitectura de Computadoras
-------------------------------------------

### Datos del alumno.
Nombre: Julio Vázquez Alvarez

No. de Cuenta: 314334398

### Detalles del Ejercicio 1 (maximoComunDivisor.asm).
Creamos dos números los cuáles son el 25 y el 5. 
Con esto buscaremos el MCD entre ellos dos (el cual es el 5).
Ocupamos un main global y dividimos el problema en el "método" GCD,
el cual hará todo el procedimiento para poder sacar el MCD.
Hacemos un método llamado salirDeGCD para usarlo como clausula de escape
para el método returnn1.

### Detalles del Ejercicio 2 (residuo.asm).
Obtenemos el resultado de la división entre 85 y 8. El procedimiento lo hacemos
por el método del algoritmo de euclides.
Toda la información del residuo se guarda en w que será el residuo.
