.data
firstnumber: .word 25
secondnumber: .word 5

.text
.globl main
main:
    lw $a0,firstnumber  
    lw $a1,secondnumber 
    jal GCD 

    add $a0,$v0,$zero 
    li $v0,1
    syscall 
li $v0, 10  
syscall

GCD:

    addi $sp, $sp, -12
    sw $ra, 0($sp) 
    sw $s0, 4($sp)  
    sw $s1, 8($sp)  

    add $s0, $a0, $zero  
    add $s1, $a1, $zero 

    addi $t1, $zero, 0
    beq $s1, $t1, returnn1

    add $a0, $zero, $s1
    div $s0, $s1
    mfhi $a1 

    jal GCD

salirDeGCD:
    lw $ra, 0 ($sp)  
    lw $s0, 4 ($sp)
    lw $s1, 8 ($sp)
    addi $sp,$sp , 12
    jr $ra
returnn1:
    add $v0, $zero, $s0 
    j salirDeGCD
