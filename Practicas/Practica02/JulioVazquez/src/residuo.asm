.data
	mensajeGlobal: .asciiz "\nEste programa regresará el resultado de 85/8.\n"
	z: .word 0 # Cociente
	w: .word 0 # Residuo
	mensajeResultado:  .asciiz "\nEl resultado de la división es: "
	mensajeResiduo: .asciiz "\nEl residuo es: "
.text 
	main:

	#Imprime el mensaje general
	li $v0, 4
	la $a0, mensajeGlobal
	syscall
	
	#index
	addi $t3, $zero, 0
	# Procedimiento por el Algoritmo de Euclides
	
	addi $t0, $zero, 85
	addi $t1, $zero, 8
	
	li $v0, 1
	move $a0, $t0
	 
	while:
		bgt $t1,$t0, exit
		sub $t0,$t0, $t1

		addi $t3, $t3, 1
	
	j while
	 
	exit:
	
	li $v0, 4
	la $a0, mensajeResultado
	syscall
	li $v0, 1
	addi $a0, $t3, 0
	syscall
	
	
	move $a0, $t0
	sw $a0, w
	li $v0, 4
	la $a0, mensajeResiduo
	syscall
	li $v0, 1
	lw $a0, w
	syscall
	
	
	# Esta línea termina el programa
	li $v0, 10
	syscall
