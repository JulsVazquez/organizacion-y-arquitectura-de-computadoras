.data
	v: .word 0x20
	w: .word 0x21
	x: .word 0x22
	y: .word 0x23
	z: .word 0x24
	resultadoSub: .word 0x1
	resultado1: .word 0x69
	resultado2: .word 0x174
	resultado3: .word 0x135
	mensaje1: .asciiz "\nEl resultado de x = (z+w)+(z+(y+(w-v)))\n"
.text
	main:
		jal solve
		
	#esta linea indical que el programa termino 
	li $v0, 10
	syscall
	
	solve:
	lw $s0, v
	lw $s1, w
	lw $s2, x
	lw $s3, y
	lw $s4, z
	lw $t1, resultadoSub
	lw $t2, resultado1
	lw $t3, resultado2
	lw $t4, resultado3
	li $v0, 4
	la $a0, mensaje1
	syscall
	
	#hacemos resta de w-v
	sub $t0, $s1, $s0
	add $a0, $zero, $t0
	add $t0, $t1, $s3
	add $a0, $zero, $t0
	add $t0, $s4, $t2
	add $a0, $zero, $t0
	add $t0, $s1, $s4
	add $a0, $zero, $t0
	add $t0, $t3, $t4
	li $v0, 1
	add $a0, $zero, $t0
	syscall	
	
	jr $ra