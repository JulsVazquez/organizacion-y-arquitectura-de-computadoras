# En data declaramos todos los datos que vamos a necesitar en .text
.data
	#declaramos un entero 
	numero2: .word 426
	#declaramos un entero
	numero1: .word 214
.text
     main:
     	jal adding


li $v0, 10
syscall

     adding:
	# Cargamos el entero numero1 al registro $s1
	lw $s1, numero1		
	# Cargamos el entero numero2 al registro $s2
	lw $s2, numero2
	# La suma de $s1 y $s2 será guardado en el registro $t1	
	add $t1, $s1, $s2
	#le indicamos que vamos a imprimir un entero	
	li $v0, 1
	# a1 = 0 + t1 esta linea de codigo sustituye la instrucción move
	# Ya que guardamos la informcación que tiene t1 en a1.
	add $a1, $zero, $t1
	syscall
	
	jr $ra
