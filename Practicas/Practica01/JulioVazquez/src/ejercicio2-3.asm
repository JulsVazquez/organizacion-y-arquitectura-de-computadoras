.data
	w: .word 0x20
	x: .word 0x21
	z: .word 0x22
	resultado: .word 0x3
	mensaje: .asciiz "x = (w-z)+(x+23)\n"
.text
	main:
		jal solve
		
	#esta linea indical que el programa termino 
	li $v0, 10
	syscall
	
	solve:
	lw $s0, w
	lw $s1, x
	lw $s2, z
	lw $s3, resultado
	# w - z
	sub $t0, $s0, $s2
	add $a0, $zero, $t0
	# x + 0x23
	add $t0, $s1, 0x23
	add $a0, $zero, $t0
	li $v0, 4
	la $a0, mensaje
	syscall
	add $t0, $s3, 0x102
	li $v0, 1
	add $a0, $zero, $t0
	syscall
	
	jr $ra
