.data
	v: .word 0x15
	mensaje: .asciiz "El resultado de v+0xef1 es: \n"
.text
	main:
		jal solve
		
	li $v0, 10
	syscall
	solve:
		lw $s0, v
		#Imprimimos message
		li $v0, 4
		la $a0, mensaje
		syscall
		#sumamos v+0xef1
		add $t0, $s0, 0xef1
		#imprimos el valor 
		li $v0, 1
		add $a0, $zero, $t0
		syscall
		
		jr $ra
