.data
	y: .word 0x19
	z: .word 0x20
	message: .asciiz "\n La suma de y + z\n"
.text
	main:
		jal adding
	#le indicamos al sistema que el programa que termino
	li $v0, 10
	syscall
	
	adding:
	#cargamos los enteros 
 	lw $s0, y
 	lw $s1, z
 	#hacemos la suma
 	add $t0, $s0, $s1
 	#imprimimos el resultado 
 	li $v0, 1
 	add $a0, $zero, $t0
 	syscall
 	
 	jr $ra
