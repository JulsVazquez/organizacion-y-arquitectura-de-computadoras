Práctica 01
===========

Organización y Arquitectura de Computadoras
-------------------------------------------

### Datos del alumno.
Nombre: Julio Vázquez Alvarez

No. de Cuenta: 314334398

### Detalles del Ejercicio 1.
Remplazamos la instrucción _move_ usando la constante cero de MIPS y la 
operación add para poder guardarlo en un registro diferente ya que la 
constante `$zero` es el neutro aditivo.

### Detalles del Ejercicio 2.
* En el archivo `ejercicio2-1.asm` se tiene el resultado de _x=y+z_
* En el archivo `ejercicio2-2.asm` se tiene el resultado de _x=(z+w+(z+(y+(w-v)))_
* En el archivo `ejercicio2-3.asm` se tiene el resultado de _x=(w-z)+(x+23)_
* En el archivo `ejercicio2-4.asm` se tiene el resultado de _x=v+0xef1_
