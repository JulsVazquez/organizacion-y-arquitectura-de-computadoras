Organización y Arquitectura de Computadoras
===========================================

Notas del 3 de Agosto del 2021
------------------------------

### La ruta de datos
Es una colección de unidades funcionales (ALU, Multiplexores, etc), registros y
buses, por los cuales una instrucción pasa para ser ejecutada.

### Program Counter
Todas las arquitecturas que tenemos de computadoras son del estilo de 
Von-Neumann.
Tenemos memoria de instrucciones y memoria de datos, el program counter nos dice
cual es la siguiente instrucción a leer o la dirección de memoria en dónde se 
encuentra dicha instrucción.

Al Program Counter siempre se le suma 4.

### Registros
Es donde guardamos la información. Dentro de ellos tenemos registros intermedios
para alimentar nuestro data-pad. 

### Memoria
Donde tenemos alineados todos los bytes en la memoria de escritura.
Es importante porque es una unidad.

### ALU
Es la que hace las operaciones. 

### Multiplexores
Cumple el que es lo que va a hacer es tomar como entradas algún registro dentro,
cumpliendo una condición.

### Registro de instrucción
Guarda el conjunto de instrucción que pasa por la memoria de instrucciones y 
asigna un registro para cada una de las instrucciones.

Ejemplo ADD R6, 320(R2)

1. R2_out, Y_in, Ytrue
2. Addr_out, add
3. Z_out,MAR_in,ReadMem
4. R6_out, Y_in, Ytrue, WMFC
5. MDR_out, add
6. Z_out, R6_in

### Controladores
* Microprogramados
  - Horizontal
  - Vertical
* Alambrados (Hardwired)
