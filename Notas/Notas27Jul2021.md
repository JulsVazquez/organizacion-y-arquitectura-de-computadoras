Notas de Arquitectura 27 de Julio de 2021
=========================================

Conjunto de Instrucciones MIPS
------------------------------

Tipo de Arquitectura:
* Load-Store

Los registros en MIPS son 32 de proposito general, 32 punto flotante.

Hay una convención como que el registro 0 debe ser 0 siempre.
El 1 es un registro para el ensamblador

El 2-3 valores

4-7 parámetros

8-15 Operaciones al sistema

16-23 Guardar valores

25-25 temporales

26-26 reservados para usar interrupciones

28 global ponter

29 stack pointer

30 saved value /frame pointer

31 return adress

### Data Types
Byte 8 Bits

Half-Word 16 bits

Word 32 bits

Double-Word 64 Bits

### Modos de direccionamiento
Opciones de Little y Big Endian

Accesos a memoria por Byte, Half-Word, Word, Double Word

Accesos deben estar alineado

### Formatos de instrucciones
l-type instruction

R-type instruction

J-type instruction

